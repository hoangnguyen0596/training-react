//====================Example 1=======================
// const root = ReactDOM.createRoot(document.getElementById('root'));
// const element = <h1>Hello, world</h1>;
// root.render(element);

//=====================Example 2======================
// function MyApp() {
//     return <h1>Hello, world!</h1>;
// }

// const container = document.getElementById('root');
// const root = ReactDOM.createRoot(container);
// root.render(<MyApp />);

//===================Example 3========================
// function Welcome(props) {
//     return <h1>Hello, {props.name}</h1>;
// }

// function App() {
//     return (
//         <div>
//             <Welcome name="Sara" />
//             <Welcome name="Cahal" />
//             <Welcome name="Edite" />
//         </div>
//     );
// }
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<App />);

//===================Example 4========================
// function formatDate(date) {
//     return date.toLocaleDateString();
// }

// function Avatar(props) {
//     return (
//         <img className="Avatar"
//             src={props.user.avatarUrl}
//             alt={props.user.name} />
//     );
// }

// function UserInfo(props) {
//     return (
//         <div className="UserInfo">
//             <Avatar user={props.user} />
//             <div className="UserInfo-name">
//                 {props.user.name}
//             </div>
//         </div>
//     );
// }

// function Comment(props) {
//     return (
//         <div className="Comment">
//             <UserInfo user={props.author} />
//             <div className="Comment-text">
//                 {props.text}
//             </div>
//             <div className="Comment-date">
//                 {formatDate(props.date)}
//             </div>
//         </div>
//     );
// }

// const comment = {
//     date: new Date(),
//     text: 'I hope you enjoy learning React!',
//     author: {
//         name: 'Hello Kitty',
//         avatarUrl: 'http://placekitten.com/g/64/64'
//     }
// };

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//     <Comment
//         date={comment.date}
//         text={comment.text}
//         author={comment.author} />
// );

//===================Example 5========================
// class Clock extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = { date: new Date() };
//     }

//     render() {
//         return (
//             <div>
//                 <h1>Hello, world!</h1>
//                 <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
//             </div>
//         );
//     }
// }

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<Clock />);


//===================Example 6========================
// class Clock extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = { date: new Date() };
//     }

//     componentDidMount() {
//         this.timerID = setInterval(
//             () => this.tick(),
//             1000
//         );
//     }

//     componentWillUnmount() {
//         clearInterval(this.timerID);
//     }

//     tick() {
//         this.setState({
//             date: new Date()
//         });
//     }

//     render() {
//         return (
//             <div>
//                 <h1>Hello, world!</h1>
//                 <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
//             </div>
//         );
//     }
// }

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<Clock />);


//===================Example 7========================
// class Toggle extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = { isToggleOn: true };

//         // This binding is necessary to make `this` work in the callback
//         this.handleClick = this.handleClick.bind(this);
//     }

//     handleClick() {
//         this.setState(prevState => ({
//             isToggleOn: !prevState.isToggleOn
//         }));
//     }

//     render() {
//         return (
//             <button onClick={this.handleClick}>
//                 {this.state.isToggleOn ? 'ON' : 'OFF'}
//             </button>
//         );
//     }
// }

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<Toggle />);


//===================Example 8========================
// class LoginControl extends React.Component {
//     constructor(props) {
//         super(props);
//         this.handleLoginClick = this.handleLoginClick.bind(this);
//         this.handleLogoutClick = this.handleLogoutClick.bind(this);
//         this.state = { isLoggedIn: false };
//     }

//     handleLoginClick() {
//         this.setState({ isLoggedIn: true });
//     }

//     handleLogoutClick() {
//         this.setState({ isLoggedIn: false });
//     }

//     render() {
//         const isLoggedIn = this.state.isLoggedIn;
//         let button;

//         if (isLoggedIn) {
//             button = <LogoutButton onClick={this.handleLogoutClick} />;
//         } else {
//             button = <LoginButton onClick={this.handleLoginClick} />;
//         }

//         return (
//             <div>
//                 <Greeting isLoggedIn={isLoggedIn} />
//                 {button}
//             </div>
//         );
//     }
// }

// function UserGreeting(props) {
//     return <h1>Welcome back!</h1>;
// }

// function GuestGreeting(props) {
//     return <h1>Please sign up.</h1>;
// }

// function Greeting(props) {
//     const isLoggedIn = props.isLoggedIn;
//     if (isLoggedIn) {
//         return <UserGreeting />;
//     }
//     return <GuestGreeting />;
// }

// function LoginButton(props) {
//     return (
//         <button onClick={props.onClick}>
//             Login
//         </button>
//     );
// }

// function LogoutButton(props) {
//     return (
//         <button onClick={props.onClick}>
//             Logout
//         </button>
//     );
// }

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<LoginControl />);

//===================Example 9========================
// function Mailbox(props) {
//     const unreadMessages = props.unreadMessages;
//     return (
//         <div>
//             <h1>Hello!</h1>
//             {unreadMessages.length > 0 &&
//                 <h2>
//                     You have {unreadMessages.length} unread messages.
//                 </h2>
//             }
//         </div>
//     );
// }

// const messages = ['React', 'Re: React', 'Re:Re: React'];

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<Mailbox unreadMessages={messages} />);

//===================Example 10========================
// function WarningBanner(props) {
//     if (!props.warn) {
//         return null;
//     }

//     return (
//         <div className="warning">
//             Warning!
//         </div>
//     );
// }

// class Page extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = { showWarning: true }
//         this.handleToggleClick = this.handleToggleClick.bind(this);
//     }

//     handleToggleClick() {
//         this.setState(prevState => ({
//             showWarning: !prevState.showWarning
//         }));
//     }

//     render() {
//         return (
//             <div>
//                 <WarningBanner warn={this.state.showWarning} />
//                 <button onClick={this.handleToggleClick}>
//                     {this.state.showWarning ? 'Hide' : 'Show'}
//                 </button>
//             </div>
//         );
//     }
// }

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<Page />);

//===================Example 11========================
// function NumberList(props) {
//     const numbers = props.numbers;
//     const listItems = numbers.map((number) =>
//         <li key={number.toString()}>
//             {number}
//         </li>
//     );
//     return (
//         <ul>{listItems}</ul>
//     );
// }

// const numbers = [1, 2, 3, 4, 5];

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<NumberList numbers={numbers} />);

//===================Example 12========================
// function ListItem(props) {
//     // Correct! There is no need to specify the key here:
//     return <li>{props.value}</li>;
// }

// function NumberList(props) {
//     const numbers = props.numbers;
//     const listItems = numbers.map((number) =>
//         // Correct! Key should be specified inside the array.
//         <ListItem key={number.toString()}
//             value={number} />
//     );
//     return (
//         <ul>
//             {listItems}
//         </ul>
//     );
// }

// const numbers = [1, 2, 3, 4, 5];

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<NumberList numbers={numbers} />);

//===================Example 13========================
// class FlavorForm extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = { value: 'coconut' };

//         this.handleChange = this.handleChange.bind(this);
//         this.handleSubmit = this.handleSubmit.bind(this);
//     }

//     handleChange(event) {
//         this.setState({ value: event.target.value });
//     }

//     handleSubmit(event) {
//         alert('Your favorite flavor is: ' + this.state.value);
//         event.preventDefault();
//     }

//     render() {
//         return (
//             <form onSubmit={this.handleSubmit}>
//                 <label>
//                     Pick your favorite flavor:
//                     <select value={this.state.value} onChange={this.handleChange}>
//                         <option value="grapefruit">Grapefruit</option>
//                         <option value="lime">Lime</option>
//                         <option value="coconut">Coconut</option>
//                         <option value="mango">Mango</option>
//                     </select>
//                 </label>
//                 <input type="submit" value="Submit" />
//             </form>
//         );
//     }
// }

// ReactDOM.render(
//     <FlavorForm />,
//     document.getElementById('root')
// );

//===================Example 14========================
// const scaleNames = {
//     c: 'Celsius',
//     f: 'Fahrenheit'
// };

// function toCelsius(fahrenheit) {
//     return (fahrenheit - 32) * 5 / 9;
// }

// function toFahrenheit(celsius) {
//     return (celsius * 9 / 5) + 32;
// }

// function tryConvert(temperature, convert) {
//     const input = parseFloat(temperature);
//     if (Number.isNaN(input)) {
//         return '';
//     }
//     const output = convert(input);
//     const rounded = Math.round(output * 1000) / 1000;
//     return rounded.toString();
// }

// function BoilingVerdict(props) {
//     if (props.celsius >= 100) {
//         return <p>The water would boil.</p>;
//     }
//     return <p>The water would not boil.</p>;
// }

// class TemperatureInput extends React.Component {
//     constructor(props) {
//         super(props);
//         this.handleChange = this.handleChange.bind(this);
//     }

//     handleChange(e) {
//         this.props.onTemperatureChange(e.target.value);
//     }

//     render() {
//         const temperature = this.props.temperature;
//         const scale = this.props.scale;
//         return (
//             <fieldset>
//                 <legend>Enter temperature in {scaleNames[scale]}:</legend>
//                 <input value={temperature}
//                     onChange={this.handleChange} />
//             </fieldset>
//         );
//     }
// }

// class Calculator extends React.Component {
//     constructor(props) {
//         super(props);
//         this.handleCelsiusChange = this.handleCelsiusChange.bind(this);
//         this.handleFahrenheitChange = this.handleFahrenheitChange.bind(this);
//         this.state = { temperature: '', scale: 'c' };
//     }

//     handleCelsiusChange(temperature) {
//         this.setState({ scale: 'c', temperature });
//     }

//     handleFahrenheitChange(temperature) {
//         this.setState({ scale: 'f', temperature });
//     }

//     render() {
//         const scale = this.state.scale;
//         const temperature = this.state.temperature;
//         const celsius = scale === 'f' ? tryConvert(temperature, toCelsius) : temperature;
//         const fahrenheit = scale === 'c' ? tryConvert(temperature, toFahrenheit) : temperature;

//         return (
//             <div>
//                 <TemperatureInput
//                     scale="c"
//                     temperature={celsius}
//                     onTemperatureChange={this.handleCelsiusChange} />
//                 <TemperatureInput
//                     scale="f"
//                     temperature={fahrenheit}
//                     onTemperatureChange={this.handleFahrenheitChange} />
//                 <BoilingVerdict
//                     celsius={parseFloat(celsius)} />
//             </div>
//         );
//     }
// }

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<Calculator />);

//====================================Example 15============================================
// function FancyBorder(props) {
//     return (
//       <div className={'FancyBorder FancyBorder-' + props.color}>
//         {props.children}
//       </div>
//     );
//   }

//   function Dialog(props) {
//     return (
//       <FancyBorder color="blue">
//         <h1 className="Dialog-title">
//           {props.title}
//         </h1>
//         <p className="Dialog-message">
//           {props.message}
//         </p>
//         {props.children}
//       </FancyBorder>
//     );
//   }

//   class SignUpDialog extends React.Component {
//     constructor(props) {
//       super(props);
//       this.handleChange = this.handleChange.bind(this);
//       this.handleSignUp = this.handleSignUp.bind(this);
//       this.state = {login: ''};
//     }

//     render() {
//       return (
//         <Dialog title="Mars Exploration Program"
//                 message="How should we refer to you?">
//           <input value={this.state.login}
//                  onChange={this.handleChange} />
//           <button onClick={this.handleSignUp}>
//             Sign Me Up!
//           </button>
//         </Dialog>
//       );
//     }

//     handleChange(e) {
//       this.setState({login: e.target.value});
//     }

//     handleSignUp() {
//       alert(`Welcome aboard, ${this.state.login}!`);
//     }
//   }

//   const root = ReactDOM.createRoot(document.getElementById('root'));
//   root.render(<SignUpDialog />);


//=============================Example 18=============================================
// const App = () => {
//     const [value, setValue] = React.useState('fruit');
//     const [options, setOptions] = React.useState([
//         { label: 'Fruit', value: 'fruit' },
//         { label: 'Vegetable', value: 'vegetable' },
//         { label: 'Meat', value: 'meat' },
//     ])

//     React.useEffect(() => {
//         setTimeout(() => {
//             setOptions(prev => [...prev, {label: 'Apple', value: 'apple'}])
//         }, 5000)
//     }, [])
//     console.log(options)

//     const handleChange = (event) => {
//         setValue(event.target.value);
//     };

//     return (
//         <div>
//             <Dropdown
//                 label="What do we eat? "
//                 options={options}
//                 value={value}
//                 onChange={handleChange}
//             />

//             <p>We eat {value}!</p>
//         </div>
//     );
// };

// const Dropdown = ({ label, value, options, onChange }) => {
//     return (
//         <label>
//             {label}
//             <select value={value} onChange={onChange}>
//                 {options.map((option) => (
//                     <option value={option.value} key={option.value}>{option.label}</option>
//                 ))}
//             </select>
//         </label>
//     );
// };

const App = () => {

    const [name, setName] = React.useState('');

    const handleCallback = (childData) => {
        setName(childData)
    }

    return (
        <div>
            <Child parentCallback={handleCallback} />
            {name}
        </div>
    )
}

const Child = (props) => {

    const onTrigger = (event) => {
        event.preventDefault();
        props.parentCallback(event.target.myname.value);
    }

    return (
        <div>
            <form onSubmit={onTrigger}>
                <input type="text" name="myname" placeholder="Enter Name" />
                <br></br><br></br>
                <input type="submit" value="Submit" />
                <br></br><br></br>
            </form>
        </div>
    )
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);