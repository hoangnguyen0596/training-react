const { useState, useEffect, useLayoutEffect, useRef, memo, useCallback, useMemo, useReducer, createContext, useContext } = React
console.log(React)

//====================================================== Example 1 ==========================================================
// function App() {

//     const [count, setCount] = useState(0);

//     const handleClick = () => {
//         setCount(count + 1)
//     }

//     return (
//         <div>
//             <p>You clicked {count} times</p>
//             <button onClick={handleClick}>
//                 Click me
//             </button>
//         </div>
//     );
// }

//====================================================== Example 2 ==========================================================
// const gifts = ['CPU i9', 'RAM 32GB RGB', 'RGB Keyboard']

// function App() {

//     const [gift, setGift] = useState()

//     const randomGift = () => {
//         const index = Math.floor(Math.random() * gifts.length)
//         setGift(gifts[index])
//     }

//     return (
//         <div style={{padding : 32}}>
//             <p>{gift || 'Chưa có phần thưởng!'}</p>
//             <button onClick={randomGift}>
//                 Lấy thưởng
//             </button>
//         </div>
//     );
// }

//-----------------------------Two-way binding-----------------------------------
// const courses = [
//     {
//         id: 1,
//         name: 'HTML/CSS'
//     },
//     {
//         id: 2,
//         name: 'JAVASCRIPT'
//     },
//     {
//         id: 3,
//         name: 'REACTJS'
//     }
// ]

// function App() {

//     const [checked, setChecked] = useState()

//     const handleSubmit = () => {
//         console.log({ id: checked })
//     }

//     return (
//         <div style={{ padding: 32 }}>
//             {
//                 courses.map( course => (
//                     <div key={course.id}>
//                         <input 
//                             type = 'radio'
//                             checked={checked === course.id}
//                             onChange = {() => setChecked(course.id)}
//                         />
//                         {course.name}
//                     </div>
//                 ))
//             }

//             <button onClick={handleSubmit}>
//                 Register
//             </button>
//         </div>
//     );
// }

//-------------------------------------------------------------------
// const courses = [
//     {
//         id: 1,
//         name: 'HTML/CSS'
//     },
//     {
//         id: 2,
//         name: 'JAVASCRIPT'
//     },
//     {
//         id: 3,
//         name: 'REACTJS'
//     }
// ]

// function App() {

//     const [checked, setChecked] = useState([])

//     console.log(checked)
//     const handleCheck = (id) => {
//         setChecked(prev => {
//             const isChekced = checked.includes(id)
//             if(isChekced){
//                 return checked.filter(item => item !== id)
//             } else {
//                 return [...prev, id]
//             }
//         })
//     }

//     const handleSubmit = () => {
//         //Call API
//         console.log({ ids: checked })
//     }

//     return (
//         <div style={{ padding: 32 }}>
//             {
//                 courses.map( course => (
//                     <div key={course.id}>
//                         <input 
//                             type = 'checkbox'
//                             checked={checked.includes(course.id)}
//                             onChange = {() => handleCheck(course.id)}
//                         />
//                         {course.name}
//                     </div>
//                 ))
//             }

//             <button onClick={handleSubmit}>
//                 Register
//             </button>
//         </div>
//     );
// }

//-------------------------------------------------------------------------
// function App() {

//     const [job, setJob] = useState('')
//     const [jobs, setJobs] = useState([])

//     const handleSubmit = () => {
//         setJobs(prev => {
//             const newJobs = [...prev, job]

//             //save localStorage
//             // localStorage.setItem('JOBS', JSON.stringify(newJobs))
//             //abc ?? []: nếu abc = null hoặc undefined thì chon cái sau

//             return newJobs
//         })
//         setJob('')

//     }

//     return (
//         <div style={{ padding: 32 }}>
//             <input 
//                 value = {job}
//                 onChange={(e) => setJob(e.target.value)}
//             />
//             <button onClick={handleSubmit}>Add</button>

//             <ul>
//                 {
//                     jobs.map((job, index) => (
//                         <li key={index}>{job}</li>
//                     ))
//                 }
//             </ul>
//         </div>
//     )
// }

//-----------------------Mounted & Unmounted----------------------------------
// function Content() {
//     return (
//         <h1> Hello anh em Ok</h1>
//     )
// }

// function App() {

//     const [ show, setShow ] = useState(false)

//     return (
//         <div style={{ padding: 32 }}>
//             <button onClick = {() => setShow(!show)}>Toggle</button>
//             {show && <Content />}
//         </div>
//     )
// }


//======================================================================== useEffect ===========================================================
//1. useEffect(callback)
// - Gọi callback mỗi khi component re-render (gọi lại liên tục)
// - Gọi callback sau khi component them element vào DOM
//2. useEffect(callback, [])
// - Chỉ gọi calback 1 lần sau khi component mounted
//3. useEffect(callback, [deps])
// - Callback sẽ được gọi lại mỗi khi deps thay đổi

//---chung của cả 3---
//1. callback luôn được gọi sau khi component mounted
//2. Cleanup function luôn được gọi trước khi component unmounted
//3. Cleanup function luôn được gọi trước khi callback được gọi (trừ lần mounted)

// const tabs = ['posts', 'comments', 'albums']

// function Content() {

//     const [ title, setTitle ] = useState('')
//     const [ posts, setPosts ] = useState([])
//     const [ type, setType ] = useState('posts')
//     const [ showGototop, setShowGototop] = useState(false)

//     //update DOM
//     //1.gọi liên tục
//     // useEffect(() =>{
//     //     fetch('https://jsonplaceholder.typicode.com/posts')
//     //         .then(res => res.json())
//     //         .then(posts => {
//     //             setPosts(posts)
//     //         })
//     // })

//     // 2.chỉ gọi 1 lần
//     // useEffect(() =>{
//     //     fetch('https://jsonplaceholder.typicode.com/posts')
//     //         .then(res => res.json())
//     //         .then(posts => {
//     //             setPosts(posts)
//     //         })
//     // }, [])

//     //3. có deps
//     useEffect(() =>{
//         fetch(`https://jsonplaceholder.typicode.com/${type}`)
//             .then(res => res.json())
//             .then(posts => {
//                 setPosts(posts)
//             })
//     }, [type])

//     //DOM event scroll
//     useEffect(() => {

//         const handleScroll = () => {
//             if(window.scrollY >= 200){
//                 setShowGototop(true)
//             }else{
//                 setShowGototop(false)
//             }
//             // setShowGototop(window.scrollY >= 200)
//         }

//         window.addEventListener('scroll', handleScroll)
//         console.log('Mounted')

//         return () => {
//             console.log('unmounting...')
//             window.removeEventListener('scroll', handleScroll)
//         }
//     }, [])

//     return (
//         <div>
//             {
//                 tabs.map(tab => (
//                     <button 
//                         key={tab}
//                         style={type === tab ? { 
//                             color: "#fff",
//                             backgroundColor: "#333"
//                          } : {}}
//                         onClick={() => setType(tab)}
//                     >{tab}</button>
//                 ))
//             }
//             <input
//                 value={title}
//                 onChange={e => setTitle(e.target.value)}
//             />
//             <ul>
//                 {
//                     posts.map(post => (
//                         <li key={post.id}>{post.title || post.name}</li>
//                     ))
//                 }
//             </ul>

//             {showGototop && 
//                 <button 
//                     style={{ 
//                         position: 'fixed',
//                         right: 20,
//                         bottom: 20
//                     }}
//                 >
//                     GoToTop
//                 </button>
//             }
//         </div>
//     )
// }

// function App() {

//     const [ show, setShow ] = useState(false)

//     return (
//         <div style={{ padding: 32 }}>
//             <button onClick = {() => setShow(!show)}>Toggle</button>
//             {show && <Content />}
//         </div>
//     )
// }

//------------------------
// function Content() {

//     const [width, setWidth] = useState(window.innerWidth)

//     useEffect(() => {

//         const handleResize = () => {
//             setWidth(width.innerWidth)
//         }

//         window.addEventListener('resize', handleResize)

//         return () => {
//             window.removeEventListener('resize', handleResize)
//         }

//     }, [])

//     return (
//         <div>
//             <h1>{width}</h1>
//         </div>
//     )
// }

// function App() {

//     const [show, setShow] = useState(false)

//     return (
//         <div style={{ padding: 32 }}>
//             <button onClick={() => setShow(!show)}>Toggle</button>
//             {show && <Content />}
//         </div>
//     )
// }

//--------------timer-------------
// function Content() {

//     const [countdown, setCountdown] = useState(180)

//     useEffect(() => {
//         const timer = setInterval(() => {
//             setCountdown(prev => prev - 1)
//             console.log('Countdown...')
//         }, 1000)

//         return () => clearInterval(timer)
//     }, [])

//     return (
//         <div>
//             <h1>{countdown}</h1>
//         </div>
//     )
// }

// function App() {

//     const [show, setShow] = useState(false)

//     return (
//         <div style={{ padding: 32 }}>
//             <button onClick={() => setShow(!show)}>Toggle</button>
//             {show && <Content />}
//         </div>
//     )
// }

//-----------------preview avatar------
// function Content() {
//     const [avatar, setAvatar] = useState()

//     useEffect(() => {
//         return () => {
//             avatar && URL.revokeObjectURL(avatar.preview)
//         }
//     }, [avatar])

//     const handlePrevAvatar = (e) => {
//         const file = e.target.files[0]

//         file.preview = URL.createObjectURL(file)

//         setAvatar(file)
//         e.target.value = null
//     }

//     return (
//         <div>
//             <input
//                 type="file"
//                 onChange={handlePrevAvatar}
//             />
//             {avatar &&
//                 <img src={avatar.preview} alt="" width="80%" />
//             }
//         </div>
//     )
// }

// function App() {

//     const [show, setShow] = useState(false)

//     return (
//         <div style={{ padding: 32 }}>
//             <button onClick={() => setShow(!show)}>Toggle</button>
//             {show && <Content />}
//         </div>
//     )
// }

//--------------------------------
//Fake comment
// function emitComment(id) {
//     setInterval(() => {
//         window.dispatchEvent(
//             new CustomEvent(`lesson-${id}`, {
//                 detail: `Nội dung comment của lesson-${id}`
//             })
//         )
//     }, 2000)
// }
// emitComment(1)
// emitComment(2)
// emitComment(3)

// const lessons = [
//     {
//         id: 1,
//         name: 'ReactJs la gi?'
//     },
//     {
//         id: 2,
//         name: 'SPA/MPA la gi?'
//     },
//     {
//         id: 3,
//         name: 'Arrow function'
//     }
// ]
// function Content() {
//     const [lessonId, setLessonId] = useState(1)

//     useEffect(() => {

//         const handleComment = ({ detail }) => {
//             console.log(detail)
//         }

//         window.addEventListener(`lesson-${lessonId}`, handleComment)

//         return () => {
//             window.removeEventListener(`lesson-${lessonId}`, handleComment)

//         }

//     }, [lessonId])

//     return (
//         <div>
//             <ul>
//                 {
//                     lessons.map(lesson => (
//                         <li
//                             key={lesson.id}
//                             style={{
//                                 color: lessonId === lesson.id ? 'red' : '#333'
//                             }}
//                             onClick={() => setLessonId(lesson.id)}
//                         >
//                             {lesson.name}
//                         </li>
//                     ))
//                 }
//             </ul>
//         </div>
//     )
// }

// function App() {

//     const [show, setShow] = useState(false)

//     return (
//         <div style={{ padding: 32 }}>
//             <button onClick={() => setShow(!show)}>Toggle</button>
//             {show && <Content />}
//         </div>
//     )
// }

//=================================================================== useLayoutEffect =========================================================
//- Update lại state
// - Update lại DOM
// - Gọi cleanup nếu deps thay đổi
// - Gọi useLayoutEffect callback
// - Render lại UI
// function Content() {
//     const [ count, setCount ] = useState(0)

//     useLayoutEffect(() => {
//         if(count > 3){
//             setCount(0)
//         }
//     }, [count])

//     const handRun = () => {
//         setCount(count + 1)
//     }

//     return (
//         <div>
//             <h1>{count}</h1>
//             <button onClick={handRun} >Run</button>
//         </div>
//     )
// }

// function App() {

//     const [show, setShow] = useState(false)

//     return (
//         <div style={{ padding: 32 }}>
//             <button onClick={() => setShow(!show)}>Toggle</button>
//             {show && <Content />}
//         </div>
//     )
// }

//=================================================================== useRef =========================================================
// function App() {

//     const [count, setCount] = useState(60)

//     const timerId = useRef()
//     const prevCount = useRef()

//     useEffect(() => {
//         prevCount.current = count
//     }, [count])

//     const handleStart = () => {
//         timerId.current = setInterval(() => {
//             setCount(prev => prev - 1)
//         }, 1000)
//     }

//     const handleStop = () => {
//         clearInterval(timerId.current)
//     }

//     // console.log(count, prevCount.current)

//     return (
//         <div style={{ padding: 32 }}>
//             <h1>{count}</h1>
//             <button onClick={handleStart}>Start</button>
//             <button onClick={handleStop}>Stop</button>
//         </div>
//     )
// }

//=================================================================== memo =========================================================
// const Content = memo(() => {
//     console.log('âssssssssssssss')
//     return (
//         <div>
//             <h1>AAAAAAAAAAAAAAAAAAAA</h1>
//         </div>
//     )
// })

// function App() {

//     const [show, setShow] = useState(1)

//     return (
//         <div style={{ padding: 32 }}>
//             <Content />
//             {show}
//             <button onClick={() => setShow(show + 1)}>Plus</button>
//         </div>
//     )
// }

//=================================================================== useCallback =========================================================
// - Tránh tạo ra những hàm mới k cần thiết.
// const Content = memo(({onIncrease}) => {
//     console.log('âssssssssssssss')
//     return (
//         <div>
//             <h1>AAAAAAAAAAAAAAAAAAAA</h1>
//             <button onClick={onIncrease}>Plus</button>
//         </div>
//     )
// })

// function App() {

//     const [show, setShow] = useState(1)

//     const handleIncrease = useCallback(() => {
//         setShow(prev => prev + 1)
//     }, [])

//     return (
//         <div style={{ padding: 32 }}>
//             <Content onIncrease = {handleIncrease} />
//             <h1>{show}</h1>
//         </div>
//     )
// }

//=================================================================== useMemo =========================================================
// function App() {
//     const [ name, setName ] = useState('')
//     const [ price, setPrice ] = useState('')
//     const [ products, setProducts ] = useState([])

//     const nameRef = useRef()

//     const handleSubmit = () => {
//         setProducts([...products, {
//             name,
//             price: +price //tương đương với parseInt
//         }])
//         setName('')
//         setPrice('')

//         nameRef.current.focus()
//     }

//     const total = useMemo(() => {
//         const result = products.reduce((result, prod) => {
//             console.log('Tính toán lại...')

//             return result + prod.price
//         }, 0)

//         return result
//     }, [products])

//     return (
//         <div style={{ padding: "10px 32px" }}>
//             <input 
//                 ref={nameRef}
//                 value={name}
//                 placeholder="Enter name..."
//                 onChange={e => setName(e.target.value)}
//             />
//             <br/>
//             <input 
//                 value={price}
//                 placeholder="Enter price..."
//                 onChange={e => setPrice(e.target.value)}
//             />
//             <br/>
//             <button onClick={handleSubmit}>Add</button>
//             <br/>
//             Total: {total}
//             <ul>
//                 {
//                     products.map((product, index) => (
//                         <li key={index}>{product.name} - {product.price}</li>
//                     ))
//                 }
//             </ul>
//         </div>
//     )
// }

//=================================================================== useReducer =========================================================
// --------------example 1------------
// useState
// 1. Init state: 0
// 2. Actions: Up( state + 1) / Down(state - 1)

// useReducer
// 1. Init state: 0
// 2. Actions: Up( state + 1) / Down(state - 1)
// 3. Reducer
// 4. Dispatch

//Init state:
// const initState = 0

// // Actions
// const UP_ACTION = 'up'
// const DOWN_ACTION = 'down'

// // Reducer
// const reducer = (state, action) => {
//     console.log("reducer running...")
//     switch(action) {
//         case UP_ACTION:
//             return state + 1
//         case DOWN_ACTION:
//             return state - 1
//         default:
//             throw new Error('Invail action')
//     }
// }

// function App() {

//     const [count, dispatch] = useReducer(reducer, initState)

//     return (
//         <div style={{ padding: "0px 20px" }}>
//             <h1>{count}</h1>
//             <button onClick={() => dispatch(DOWN_ACTION)}>Down</button>
//             <button onClick={() => dispatch(UP_ACTION)}>Up</button>
//         </div>
//     )
// }

//------------------------------example 2---------
// innit state
// const initState = {
//     job: '',
//     jobs: []
// }

// // Action
// const SET_JOB = 'set_job'
// const ADD_JOB = 'add_job'
// const DELETE_JOB = 'delete_job'

// const setJob = payload => {
//     return {
//         type: SET_JOB,
//         payload
//     }
// }

// const addJob = payload => {
//     return {
//         type: ADD_JOB,
//         payload
//     }
// }

// const deleteJob = payload => {
//     return {
//         type: DELETE_JOB,
//         payload
//     }
// }

// // reducer
// const reducer = (state, action) => {
//     switch (action.type) {
//         case SET_JOB:
//             return {
//                 ...state,
//                 job: action.payload
//             }
//         case ADD_JOB:
//             return {
//                 ...state,
//                 jobs: [...state.jobs, action.payload]
//             }
//         case DELETE_JOB:
//             const newJobs = [...state.jobs]

//             newJobs.splice(action.payload, 1)
//             return {
//                 ...state,
//                 jobs: newJobs
//             }
//         default:
//             throw new Error("Invalid action")
//     }
// }

// function logger(reducer) {
//     return (prevState, action) => {
//         console.group(action.type)
//         console.log('Prev State: ', prevState)
//         console.log('Action: ', action)

//         const newState = reducer(prevState, action)

//         console.log('Next state: ', newState)

//         return newState
//     }
// }

// function App() {

//     const [state, dispatch] = useReducer(reducer, initState)
//     const { job, jobs } = state
//     const inputRef = useRef()

//     const handleSubmit = () => {
//         dispatch(addJob(job))
//         dispatch(setJob(''))

//         inputRef.current.focus()
//     }

//     return (
//         <div style={{ padding: "0px 20px" }}>
//             <h3>Todo</h3>
//             <input
//                 value={job}
//                 placeholder="Enter todo..."
//                 onChange={e => {
//                     dispatch(setJob(e.target.value))
//                 }}
//                 ref={inputRef}
//             />
//             <button onClick={handleSubmit}>Add</button>
//             <ul>
//                 {
//                     jobs.map((job, index) => (
//                         <li key={index}>
//                             {job}
//                             <span onClick={() => dispatch(deleteJob(index))}>&times;</span>
//                         </li>
//                     ))
//                 }
//             </ul>
//         </div>
//     )
// }

//=================================================================== useContext =========================================================
// 1. Create Context
// 2. Provider
// 3. Consumer
const ThemeContext = createContext()
console.log(ThemeContext)

function Paragraph() {
    const theme = useContext(ThemeContext)

    return (
        <p className={theme}>KKKKKKOOKOKOKOOOOOOOOO</p>
    )
}

function Content() {
    return (
        <div>
            <Paragraph />
        </div>
    )
}

function App() {
    const [theme, setTheme] = useState('dark')

    const toggleTheme = () => {
        setTheme(theme === 'dark' ? 'light' : 'dark')
    }

    return (
        <ThemeContext.Provider value={theme}>
            <div style={{ padding: 32 }}>
                <Content/>
                <button onClick={toggleTheme}>Toggle</button>
            </div>
        </ThemeContext.Provider>
    )
}



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
